#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
// This C file defines functions that will
// be used from .ss files through the
// gambit FFI
// See the following documentation
// https://cons.io/guide/ffi.html#basic-ffi
// http://www.iro.umontreal.ca/~gambit/doc/gambit.html#C_002dinterface
// http://gambitscheme.org/wiki/index.php?title=Using_Gambit_with_External_Libraries&oldid=2674
// The whole thread at
// https://mailman.iro.umontreal.ca/pipermail/gambit-list/2013-September/007013.html
// This file is included in tz-conversion.ss to expose
// its functions to code written in scheme.

char answer[21];  // The return string will be written in this buffer,
// a copy of which is going to be made by the C-interface of gambit
// and returned to scheme

char* tz_to_utciso(___S32 year, ___S32 month,
                 ___S32 day, ___S32 hour,
                 ___S32 minute, ___S32 second,
                 char* tz){
  // Set answer to the UTC datetime in ISO format
  // that is equal to the given broken down date
  // (e.g. tz_to_epoch(2020, 12, 25, 14, 42, 13, ...)
  // for christmas day 2020 at 14:42:13).
  // in the specified timezone.
  // answer is large enough to accomodate
  // an iso formated date: YYYY-MM-DDThh:mm:ssZ, i.e. 21 bytes
  // including the null termination
  struct tm input;
  input.tm_isdst = -1;  // We don't know if DST was in effect
  input.tm_year = year - 1900;
  input.tm_mon = month - 1;  // Months are 0 indexed
  input.tm_mday = day; // But days aren't lol
  input.tm_hour = hour;
  input.tm_min = minute;
  input.tm_sec = second;
  setenv("TZ", tz, 1);
  time_t epoch_time = mktime(&input);
  strftime(answer, 21, "%Y-%m-%dT%H:%M:%SZ", gmtime(&epoch_time));
  return answer;
}

// The following commented main function allows the testing
// of tz_to_utciso directly on the command line
// #define ___S32 int
/* int main(int argc, char** argv){ */
/*   tz_to_utciso( */
/*                atoi(argv[2]), */
/*                atoi(argv[3]), */
/*                atoi(argv[4]), */
/*                atoi(argv[5]), */
/*                atoi(argv[6]), */
/*                atoi(argv[7]), */
/*                argv[1] */
/*                ); */
/*   printf("Converted to utciso: %s\n", answer); */
/*   return 0; */
/* } */
