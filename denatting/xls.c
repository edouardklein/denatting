#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <xls.h>

// Global state variables
// Not thread safe, you're supposed to read only one file at a time
xlsWorkBook* pWB = NULL;
xlsWorkSheet* pWS;
xls_error_t code = LIBXLS_OK;
struct st_row_data* row;
WORD row_index;
WORD cell_index;
unsigned char *data = NULL;

void xls_open_memory(___SCMOBJ vec, size_t len){
  if (pWB != NULL) {
    xls_close_WB(pWB);
    xls_close_WS(pWS);
    pWB = NULL;
    pWS = NULL;
  }
  //pWB=xls_open_file(fname, "UTF-8", &code);
  // Initialize from a buffer
  // First we copy the buffer so that scheme's GC
  // can't make it invalid
  if(data != NULL){
    //Reclaim memory when a new file is opened
    free(data);
    data=NULL;
  }
  data = malloc(len);
  if(data == NULL){
    fprintf(stderr, "Error allocating memory\n");
    return;
  }
  memcpy(data, ___BODY(vec), len);
  pWB=xls_open_buffer(data, len, "UTF-8", &code);
  //
  if (pWB == NULL) {
    fprintf(stderr, "Unable to open: %s\n",
            xls_getError(code));
    return;
  }
  pWS=xls_getWorkSheet(pWB,0);
  if ((code = xls_parseWorkSheet(pWS)) != LIBXLS_OK) {
    fprintf(stderr, "Error parsing worksheet: %s\n", xls_getError(code));
    return;
  }
  row_index = 0;
  cell_index = -1;
  return;
}

char* xls_next_value(){
  cell_index++;
  if (cell_index > pWS->rows.lastcol){
    cell_index = 0;
    row_index++;
  }
  if (row_index > pWS->rows.lastrow){
    return NULL;
  }
  if(pWS->rows.row[row_index].cells.cell[cell_index].str != NULL){
    return pWS->rows.row[row_index].cells.cell[cell_index].str;
  } else {
    return "";
  }
}

/* int main(int argc, char *argv[]) */
/* { */
/*   xls_open_memory(argv[1], 42); */
/*   char* cell_contents = xls_next_value(); */
/*   while(cell_contents != NULL){ */
/*     printf("%s\n", cell_contents); */
/*     cell_contents = xls_next_value(); */
/*   } */
/*   return 0; */
/* } */
