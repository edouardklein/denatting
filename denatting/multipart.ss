(import
 "utils"
 :std/format
 :std/misc/repr
 :std/net/httpd
 :gerbil/gambit/bytes
 :std/misc/list
 :std/iter
 :std/pregexp
  )
(export #t)

(def (http-headers->delimiter headers)
     ;; Return the boundary delimiter line given in the header or #f
     ;; if there is no Content-Type header
     ;; headers is something like
     ;; [["Host" . "127.0.0.1:8080"]
     ;; ["User-Agent" . "curl/7.70.0"]
     ;; ["Accept" . "*/*"]
     ;; ["Content-Length" . "373"]
     ;; ["Content-Type" . "multipart/form-data; boundary=------------------------25ef4f5c9e83664a"]]
     (if-let ((content-type  (assget "Content-Type" headers)))
             ;; content-type is something like
             ;; multipart/form-data; boundary=------------------------25ef4f5c9e83664a
             (-> content-type
                 (string-split #\;)
                 (list-ref 1)
                 (string-split #\=)
                 (list-ref 1)
                 (string->bytes))
             #f))

(def (delimiter-end data delimiter index)
     ;; Return the index at which the delimiting pattern that starts at index
     ;; ends or #f if a delimiting pattern is not starting at index.
     ;; A delimiter is supposed to be prefixed by two hyphens
     ;; and a new line.
     ;; Despite the RFC stating that
     ;; "The requirement that the encapsulation boundary begins with a CRLF
     ;; implies that the body of a multipart entity must itself begin with
     ;; a CRLF before the first encapsulation line" we accept a delimiting
     ;; pattern that does not start with a new line, as modern browsers
     ;; do not insert an empty line before the body.
     ;; The (optional) new line and the two hyphens are considered
     ;; to be part of the delimiting pattern. The new line after the delimiter
     ;; is also considered as a part of the delimiting pattern.
     (let ((L (bytes-length delimiter))
           (data-length (bytes-length data)))
       (cond
        ((> (+ index 4 L 2) data-length) #f); End of data
        ((and
          ;; a CRLF before the delimiter is part of the delimiter
          (equal? (subbytes data    index      (+ index 4))     #u8(13 10 45 45))
          (equal? (subbytes data (+ index 4)   (+ index 4 L))   delimiter)
          (equal? (subbytes data (+ index 4 L) (+ index 4 L 2)) #u8(13 10)))
         (+ index 4 L 2))
        ((and
          ;;But the first delimiter may start without a leading CRLF
          (= index 0)
          (equal? (subbytes data    index      (+ index 2))     #u8(45 45))
          (equal? (subbytes data (+ index 2)   (+ index 2 L))   delimiter)
          (equal? (subbytes data (+ index 2 L) (+ index 2 L 2)) #u8(13 10)))
         (+ index 2 L 2))
        ((and
          ;; The last delimiter has a trailing -- before the CRLF
          ;; But there may not be a last CRLF after the trailing --
          (equal? (subbytes data    index      (+ index 4))     #u8(13 10 45 45))
          (equal? (subbytes data (+ index 4)   (+ index 4 L))   delimiter)
          (equal? (subbytes data (+ index 4 L) (+ index 4 L 2)) #u8(45 45)))
         (+ index 4 L 2))
        (else #f))))

(def (delimiter-limits data delimiter)
     ;; Return the list of [start end] indexes of the delimiters
     (for/fold (limits []) (index (iota (bytes-length data)))
               (if-let ((end (delimiter-end data delimiter index)))
                       (snoc [index end] limits)
                       limits)))

(def (parts-limits data delimiter)
     ;; Return the list of [start end] indexes of the parts
     ;; We just take the indices between the end of a delimiter
     ;; and the start of the next.
     ;; This will ignore anything before the first delimiter
     ;; and anything after the last, which is how it should be
     ;; see RFC1341
     (let* ((dlims  (delimiter-limits data delimiter))
            (fdlims (flatten dlims))
            ;; Cut head and tail
            (inner-posts (slice fdlims 1 (- (length fdlims) 2))))
       ;; Group two by two
       (for/fold (answer []) (i (iota (/ (length inner-posts) 2) 0 2))
                 (snoc (slice inner-posts i 2) answer))))

(def (end-of-headers part)
     ;;Return the index of the end of the headers
     ;; of a part (including the two CRLF at the end)
     (do ((index 0 (+ 1 index)))
         ((equal? (subbytes part index (+ index 4)) #u8(13 10 13 10))
          (+ index 4))))

(def (part-name headers)
     ;; Return the name of a part as specified in the given headers
     (cadr (pregexp-match ".*; name=\"([^\"]*)\".*" headers)))

(def (http-request->hash req)
     ;; Parse a multipart/form-data http request into a hash table
     ;; Whose keys are the values of the name field of each part
     ;; and whose values are the associated bytes content.
     ;; See https://www.w3.org/Protocols/rfc1341/7_2_Multipart.html
     ;; For the documentation that helped me write this piece of code
  (if-let ((delimiter (http-headers->delimiter (http-request-headers req))))
          (let* ((payload (http-request-body req))
                 (limits (parts-limits payload delimiter)))
            (plist->hash-table
             (for/fold
              (l [])
              ((limit limits))
              (let* ((part (apply (cut subbytes payload <...>) limit))
                     (frontier (end-of-headers part))
                     (part-headers (bytes->string (subbytes part 0 frontier)))
                     (part-payload (subbytes part frontier (bytes-length part))))
                (append [(part-name part-headers) part-payload] l)))))
          (make-hash-table)))
