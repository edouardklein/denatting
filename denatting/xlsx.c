#include <stdio.h>
#include <xlsxio_read.h>
#include <string.h>
// This C file defines functions that will
// be used from .ss files through the
// gambit FFI
// See the following documentation
// https://cons.io/guide/ffi.html#basic-ffi
// http://www.iro.umontreal.ca/~gambit/doc/gambit.html#C_002dinterface
// http://gambitscheme.org/wiki/index.php?title=Using_Gambit_with_External_Libraries&oldid=2674
// The whole thread at
// https://mailman.iro.umontreal.ca/pipermail/gambit-list/2013-September/007013.html
// This file is included in xlsx.ss to expose its functions
// to code written in scheme.
// Due to the use of global variables, only
// one file can be read at a time.
// Wrap in a (with-lock) block.

// These global variables are the context
// needed to read an excel file
xlsxioreader xlsxioread; // The file handle
xlsxioreadersheet sheet; // The sheet handle
char* value = NULL;  // The value of a cell
void* data = NULL;  // The xlsx data

int open_sheet(){
  // Once the xlsxioreader is initialized, open
  // the fist sheet.
  if ((sheet = xlsxioread_sheet_open(xlsxioread, NULL,
                                     XLSXIOREAD_SKIP_EMPTY_ROWS))
      == NULL) {
    fprintf(stderr, "Error opening the first sheet\n");
    return 1;
  }
  if (xlsxioread_sheet_next_row(sheet) == 0) {
    fprintf(stderr, "Error opening the first row\n");
    return 1;
  }
  return 0;
}

int xlsx_open_memory(___SCMOBJ vec, size_t len){
  // Initialize the xlsxioreader from a buffer
  // First we copy the buffer so that scheme's GC
  // can't make it invalid
  if(data != NULL){
    free(data);
    data=NULL;
  }
  data = malloc(len);
  if(data == NULL){
    fprintf(stderr, "Error allocating memory\n");
    return 1;
  }
  memcpy(data, ___BODY(vec), len);
  // Then we use xlsxio to read from that buffer
  if ((xlsxioread = xlsxioread_open_memory(data, len, 0)) == NULL) {
    fprintf(stderr, "Error opening .xlsx file from memory\n");
    return 1;
  }
  return open_sheet();
}

char* next_value(){
  // Return the value of the next cell
  if (value != NULL) {
    free(value);
  }
  value = xlsxioread_sheet_next_cell(sheet);
  if (value != NULL) {
    return value;
  }
  if (xlsxioread_sheet_next_row(sheet) == 0) {
    return NULL;
  }
  return next_value();
}
