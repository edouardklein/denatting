(import
 "core"
 "utils"
 "tz-str-date"
 :std/srfi/19
 :std/pregexp
 :gerbil/gambit/os
  )
(export #t)

;; ORANGE
(def orange-fortinet-log-re
  ;; Matches the firewall log part of an orange fortinet CSV
  (string-append
   ".*"                                    ; Some bullshit
   "date=(\\d\\d\\d\\d-\\d\\d-\\d\\d) "    ; Date
   "time=(\\d\\d:\\d\\d:\\d\\d)"           ; Time
   ".*"                                    ; Some more bullshit
   "dstip=(" ip-re "|" ipv6 ") "           ; Destination IP (may be ipv6)
   "dstport=(\\d+)"                        ; Destination port
   ".*"                                    ; Some more bullshit
   "transip=(" ip-re ") "                  ; Source IP
   "transport=(\\d+)"                      ; Source port
   ))

(def (fr-orange-fortinet-line->straw line)
     ;; Return a straw from an Orange CSV line
     (with ([id _ _ _ _ _ _ _ _ _ _ fw-log]
         (string-split line #\;))
    (with ([_ date-str time-str dest-ip dest-port src-ip src-port]
           (pregexp-match orange-fortinet-log-re fw-log))
      (make-straw
       id
       src-ip
       src-port
       dest-ip
       dest-port
       (datetime-str->datetime (string-append date-str time-str)
                               "~Y-~m-~d~H:~M:~S"
                               "Europe/Paris")))))
