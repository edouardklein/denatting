(import
 "core"
 "xls"
 "utils"
 "tz-str-date"
 :gerbil/gambit/bytes
 :std/format
 :std/misc/repr
 :gerbil/gambit/threads
 )
(export #t)

;; PT-MEO
(def (remove-nac-private-bullcrap nac-id)
     ;; Return the id without the "@internet-nac-private"
     ;; stuff that meo adds at the end for whatever reason
     (car (string-split nac-id #\@)))

(def (pt-meo-line->straw line)
     ;; Return a straw from a meo xls line
     (with ([src-ip _ _ _ xls-datetime _ _ nac-id] line)
           (make-straw
            (remove-nac-private-bullcrap nac-id)
            src-ip
            #f  ; no src port
            #f  ; no dst ip
            #f  ; no dst port
            (datetime-xls->datetime
             (string->number xls-datetime) "Europe/Lisbon"))))


(def (pt-meo-file->lines contents)
     (with-lock (make-mutex)
                (lambda ()
                  ;; Return lines from the contents of a portugese meo xls file
                  (xls-open-memory contents (bytes-length contents))
                  ;; Discard the headers
                  (xls-next-values 8)
                  ;; Return the rest
                  (all-remaining-values 8 next-func: xls-next-values))))
