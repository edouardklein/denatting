(define-module (denatting)
  #:use-module (gnu system)
  #:use-module (gnu system vm)
  #:use-module (gnu system file-systems)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services shepherd)
  #:use-module (beaver packages scheme-xyz)
  ;#:use-module (oop goops)
  #:use-module (guix gexp)
  ;#:use-module (guix records)
  ;#:use-module (ice-9 match)
  #:export (denatting-service))

(define denatting-service
  (lambda (_)
    (list
     (shepherd-service
      (provision '(denatting))
      (documentation "Denatting web server")
      (respawn? #t)
      (start #~((make-forkexec-constructor
                 (list "denat")
                 #:log-file "/tmp/denatting.log")))
      (stop #~((make-kill-destructor)))))))

(define denatting-service-type
  (service-type
   (name 'denatting)
   (extensions
    (list (service-extension shepherd-root-service-type denatting-service)))
   (default-value #f)))


(virtualized-operating-system
 (operating-system 
  (host-name "denatting")
  (timezone "Europe/Paris")
 (bootloader (bootloader-configuration
              (bootloader grub-bootloader)))
  (packages (append (list
                     gerbil-denatting)
                    %base-packages))
  (file-systems %base-file-systems)
;  (file-systems (append (list
;                         (file-system
;                          (mount-point "/")
;                          (device (file-system-label "moncul"))
;                          (type "ext4")))
;                        %base-file-systems))
  (services (append (list
                     (service denatting-service-type))
                    %base-services)))
 '())
