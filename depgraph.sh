#!//bin/bash
echo 'digraph G {' > depgraph.dot
for f in denatting/*.ss
do
    for i in $(awk '/\(import/,/\)/' $f | grep -E '^ +".*"$')
    do
        echo '"'$(basename $f .ss)'"' '->' $i ';'
    done
done >> depgraph.dot
echo '}' >> depgraph.dot
dot -Tpng depgraph.dot > depgraph.png
